#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import socket
from thread import *
import subprocess

def client_thread(conn):
    #conn.send('this is server\n')
    
    data = conn.recv(1024)
    print('recive:',data)
    reply = dosomething(data)
    print 'reply:', reply
    conn.send(reply)
    conn.close()
    print('server close')

def dosomething(cmd):
    p = subprocess.Popen(cmd,
                            stdin = subprocess.PIPE,
                            stdout = subprocess.PIPE,
                            stderr = subprocess.PIPE,
                            shell = True)
    return ''.join(p.communicate())

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(('192.168.70.71',9879)) 
server.listen(5)

while True:
    print ('wait connect')
    conn,addr = server.accept() 
    print(conn,addr)
    start_new_thread(client_thread, (conn,))
    
    '''data = str(conn.recv(1024))  
    print('recive:',data)  
    conn.close()
    print 'server close'
    '''
