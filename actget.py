#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import sys, os, time

Astate_path = '/home/pi/barcode/statusA'
Bstate_path = '/home/pi/barcode/statusB'
pin_def = {'A':Astate_path, 'B':Bstate_path}

def actget():
    def statusGet():
        vala = os.popen('cat /home/pi/barcode/statusA').read().decode().strip()
        valb = os.popen('cat /home/pi/barcode/statusB').read().decode().strip()
        valc = os.popen('cat /home/pi/barcode/pos').read().encode('utf8').strip() 
        val_ = int(vala+valb, 2)
        #print (val_, valc)
        return (val_, valc)

    def picmd(cmd, prompt):
        stdout = os.popen(cmd).read()
        if prompt in stdout:
            return stdout
        else:
            return 'err'


    while True:
        val,pos = statusGet()
        if val == 1 or val == 13: # 1:0001, 13:1101
            if pos != 'AB':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:0 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            os.popen('echo 10 > /home/pi/barcode/statusB')
            print 'flowa'
            return
        
        if val == 4 or val == 7: # 4:0100, 7:0111
            if pos != 'BA':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:180 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            os.popen('echo 10 > /home/pi/barcode/statusA')
            print 'flowa'
            return

        elif val == 6 or val == 9: # 6:0110, 9:1001
            if val == 6 and pos != 'AB':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:0 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            if val == 9 and pos != 'BA':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:180 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            print 'flowb'
            return


        elif val ==3 or val == 11 or val==8: # 3:0011, 11:1011, 8:1000
            if pos != 'BA':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:180 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            print 'start'
            return

        elif val == 12 or val == 14 or val == 2: # 12:1100, 14:1110, 2:0010
            if pos != 'AB':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:0 5', '1}')
                picmd('fixture CYLINDER:ON 5', '1')
            print 'start'
            return
        
        elif val == 0: 
            #picmd('fixture CYLINDER:OFF 5', '1')
            Astat = str(os.popen('fixture IRSENSORA:GET 10').read().split(',')[-1].split('}')[0])
            Bstat = str(os.popen('fixture IRSENSORB:GET 10').read().split(',')[-1].split('}')[0])
            stat = Astat + Bstat
            if stat == '00' or stat =='11':
                print 'done'
                return
            elif stat == '01' and pos == 'AB':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:180 5', '1}')
                print 'B fuck exist'
                return
            elif stat == '10' and pos == 'BA':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:0 5', '1}')
                print 'A fuck exist'
                return
            else:
                print 'something on outside, fuckin taking it away right now'
                return

            '''if pos != 'AB':
                picmd('fixture CYLINDER:OFF 5', '1')
                picmd('fixture MOTOR:0 5', '1}')
               return'''
            

        else:
            time.sleep(1)
            pass

if __name__ == '__main__':
    actget()
