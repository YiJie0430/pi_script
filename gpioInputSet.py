#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from time import sleep
import RPi.GPIO as GPIO
import time
import os, sys

def ioInput(gpioPin, act):
    GPIO.setmode(GPIO.BCM)
    if act:
        act = GPIO.PUD_UP
    else:
        act = GPIO.PUD_DOWN

    GPIO.setup(gpioPin, GPIO.IN, pull_up_down=act)

    input_last_value = None
    while True:
            input_value = GPIO.input(gpioPin)
            if input_value != input_last_value:
                print ('Input value on PIN {} change to:'.format(gpioPin), input_value)
                input_last_value = input_value
            else:
                break
            time.sleep(1)

if __name__ == '__main__':
     try: 
        ioInput(int(sys.argv[1]), int(sys.argv[2])) 
     except KeyboardInterrupt:
        GPIO.cleanup()

