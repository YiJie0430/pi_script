#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import socket

def pLock(processName):
    pLock.lockSocket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        # The null byte (\0) means the socket is created
        # It works only in linux
        pLock.lockSocket.bind('\0' + processName)
        return False
    except socket.error:
        return True
