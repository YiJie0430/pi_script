#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import os
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import socket
from thread import *
import threading
import subprocess

class tcp_socket:
    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.server.bind((hostname, port))
        self.server.listen(5)
        self.conn = None
        self.addr = None

    def _link(self):
        self.conn, self.addr = self.server.accept()
        return self.conn, self.addr

def client_thread(host, port, conn):        
    def dosomething(cmd):
        p = subprocess.Popen(cmd,
                        stdin = subprocess.PIPE,
                        stdout = subprocess.PIPE,
                        stderr = subprocess.PIPE,
                        shell = True)
        return ''.join(p.communicate())
    
    conn.send('{}:{} listening'.format(host, port))
    data = conn.recv(1024)
    print('recive:',data)
    reply = dosomething(data)
    print ('reply:', reply)
    conn.send(reply)
    conn.close()
    print('server close')



if __name__ == '__main__':
    #host = '192.168.70.70'
    #port = 9179
    host = sys.argv[1]
    port = int(sys.argv[-1])
    print ('host:', sys.argv[1], 'port:', sys.argv[-1])
    server = tcp_socket(host, port)
    while True:
        print ('wait connect') 
        conn,addr = server._link() 
        print(conn,addr)
        start_new_thread(client_thread, (host,port,conn,))
