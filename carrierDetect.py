#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import sys, os, time, getopt
import RPi.GPIO as GPIO
from threading import Thread
reload(sys)
sys.setdefaultencoding('utf8')

Astate_path = '/home/pi/barcode/statusA'
Bstate_path = '/home/pi/barcode/statusB'
pin_def = {'A':[12,22,Astate_path], 'B':[13,24,Bstate_path]}

def gpio_deb_state( a_idx ):

    gpio_deb_src_tim = time.time()
    gpio_deb_dif_tim = 0
    gpio_deb_tmp = 0xff

    while True:
        gpio_deb_trg_tim = time.time()
        gpio_deb_dif_tim = gpio_deb_trg_tim - gpio_deb_src_tim
        
        if ( GPIO.input( pin_def[a_idx][0] ) == gpio_deb_tmp ) :
            if ( GPIO.input( pin_def[a_idx][0] ) != gpio_deb_tmp ) :
                gpio_deb_src_tim = time.time()
                gpio_deb_dif_tim = 0
                gpio_deb_tmp = 0xff
                continue
            elif gpio_deb_dif_tim >= 0.1:
                return gpio_deb_tmp
        elif ( GPIO.input( pin_def[a_idx][0] ) == GPIO.HIGH ) :
            gpio_deb_tmp = GPIO.HIGH
            print ('high')
        elif ( GPIO.input( pin_def[a_idx][0] ) == GPIO.LOW )  :
            gpio_deb_tmp = GPIO.LOW
            print ('low')
def detect(idx):
    def pwr_on(pin, path):
        scan = os.popen('barcode "04E40400FF14" 1').read()
        print (scan)
        if 'CR' in scan:
            mac = scan.split(',')[-1].strip()
            if idx == 'A':
                os.popen('echo 1{}:{},1 > /home/pi/barcode/output'.format(idx,mac))
            else:
                os.popen('echo 1{}:{},2 > /home/pi/barcode/output'.format(idx,mac))
            print (os.popen('fixture HOLDER{}:ON 5'.format(idx)).read())
            print (os.popen('gpioset -p {} -s on'.format(pin)).read())
            os.popen('echo 01 > {}'.format(path))
 
    def pwr_off(pin, path):
        print (os.popen('gpioset -p {} -s off'.format(pin)).read())
        print (os.popen('fixture HOLDER{}:OFF 10'.format(idx)).read())
        os.popen('echo 00 > {}'.format(path))
        os.popen('echo '' > /home/pi/barcode/output')
   
    GPIO.setmode(GPIO.BCM)
    #GPIO.setup(pin_def[idx][0], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    #GPIO.setup(pin_def[idx][0], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(pin_def[idx][0], GPIO.IN, pull_up_down=GPIO.PUD_OFF)
    GPIO.setwarnings(False)

    try:
        trigger = 0
        status = False
        while True:
            try:
                if gpio_deb_state(idx) == GPIO.LOW:
                    print ('{} detected'.format(idx))
                    state = int(os.popen('cat {}'.format(pin_def[idx][-1])).read(),2)
                    if state == 0 and not trigger:
                        time.sleep(0.1)
                        if GPIO.input(pin_def[idx][0]) == GPIO.LOW:
                            status = int(os.popen('fixture IRSENSOR{}:GET 10'.format(idx)).read().split(',')[-1].split('}')[0])
                            if status:
                                pwr_on(pin_def[idx][1], pin_def[idx][-1])
                                trigger = 1
                    elif state == 0 and trigger == 1:
                        pwr_off(pin_def[idx][1], pin_def[idx][-1])
                        trigger = 2
                    else:
                        print ('{} not thing to do'.format(idx))
                        pass
                    time.sleep(1)
                else:
                    print ('{} not detected'.format(idx))
                    if trigger:
                        os.popen('echo 00 > {}'.format(pin_def[idx][-1]))
                        trigger = 0
                    time.sleep(1)
            except Exception as err:
                status = False
                print (err)
                time.sleep(1)
            
    except KeyboardInterrupt:
        print ('close')
        os.popen('echo 00 > {}'.format(pin_def[idx][-1]))
        GPIO.cleanup()
    finally:
        GPIO.cleanup()

if __name__ == '__main__':
    carrier = sys.argv[1]
    detect(carrier)
    
