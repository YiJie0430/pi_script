'''
from SimpleCV import Color,Camera,Display

cam = Camera()  #starts the camera
display = Display() 

while(display.isNotDone()):
    img = cam.getImage() #gets image from the camera
    barcode = img.findBarcode() #finds barcode data from image
    if(barcode is not None): #if there is some data processed
        barcode = barcode[0] 
        result = str(barcode.data)
        print result #prints result of barcode in python shell
        barcode = [] #reset barcode data to empty set
        img.save(display) #shows the image on the screen '''


# -*- coding:utf-8 -*-
import zbar
from PIL import Image
scanner = zbar.ImageScanner()
scanner.parse_config('enable')
img = Image.open('/home/pi/PythonCode/test2.jpg').convert('L')
width, height = img.size
qrCode = zbar.Image(width, height, 'Y800', img.tobytes())
print qrCode
scanner.scan(qrCode)
data = ''
for s in qrCode:
    print s
    data  = s.data
    #del img
    print data 

'''
#!/usr/bin/python
from sys import argv
import zbar

# create a Processor
proc = zbar.Processor()

# configure the Processor
proc.parse_config('enable')

# initialize the Processor
device = '/dev/video0'
if len(argv) > 1:
        device = argv[1]
        proc.init(device)
        print 'yes'
        # enable the preview window
        proc.visible = True

        # read at least one barcode (or until window closed)
        proc.process_one()

        # hide the preview window
        proc.visible = False

        # extract results
        for symbol in proc.results:
                # do something useful with results
                print 'decoded', symbol.type, 'symbol', '"%s"' % symbol.data '''
