import serial, time

class cyt_fixture():
    def __init__(self):
        self.ser=serial.Serial('/dev/ttyUSB0', 9600)
        self.ser.bytesize=7
        self.ser.parity='E'
        self.opt = {'A':'1', 'B':'2', 'run':'YELLOW',
                    'pass':'GREEN', 'fail':'RED',
                    'on':'ON', 'off':'OFF'}
    def _init(self):
        self.ser.write(b'MACHINE:HOME')

    def start(): #holderA and holderB on
        self.ser.write(b'MACHINE:START')
        
    def holder(self, idx, act):
        self.ser.write(b'HOLDER{}:{}'.format(idx, act))

    def rotate(self, pos):
        self.ser.write(b'MOTOR:{}'.format(pos))

    def cylinder(self, act):
        self.ser.write(b'TESTCYLINDER:{}'.format(act))
        time.sleep(1)

    def led(self, idx, status, act):
        for state in ['RED', 'YELLOW', 'GREEN']:
            self.ser.write(b'LIGHT{}{}:OFF'.format(self.opt[idx],
                                                  state))
            time.sleep(0.1)

        self.ser.write(b'LIGHT{}{}:{}'.format(self.opt[idx],
                                             self.opt[status],
                                             self.opt[act]))

if __name__ == '__main__':
    cyt=cyt_fixture()
    print ('fixture init')
    cyt._init()
    time.sleep(20)
    print ('init finish')
    for loop in range(100):
        print 'loop time: ', loop
        
        print 'A -', loop
        cyt.holder('A','ON')
        time.sleep(1)
        cyt.holder('B','ON')
        time.sleep(1)
        
        print 'A - running', loop
        cyt.led('A','run', 'on')
        time.sleep(1)

        cyt.rotate(180)
        time.sleep(5)

        print 'cylinder on -', loop
        cyt.cylinder('ON')
        time.sleep(3)

        cyt.holder('B','OFF')
        time.sleep(0.5)
        
        if loop >= 1:
            if loop % 2:
                print 'B - pass', loop
                cyt.led('B','pass', 'on')
                time.sleep(0.1)
            else:
                print 'B - fail', loop
                cyt.led('B','fail', 'on')
                time.sleep(0.1)

        print 'cylinder off -', loop
        cyt.cylinder('OFF')
        time.sleep(3)

        print 'B -', loop
        cyt.holder('B','ON')
        time.sleep(1)
        
        print 'B - running', loop
        cyt.led('B','run', 'on')
        time.sleep(1)


        cyt.rotate(0)
        time.sleep(5)
        
        
        print 'cylinder on -', loop
        cyt.cylinder('ON')
        time.sleep(3)

        cyt.holder('A','OFF')
        time.sleep(1)
        
        if not (loop % 2):
            print 'A - pass', loop
            cyt.led('A','pass', 'on')
            time.sleep(1)
        else:
            print 'A - fail', loop
            cyt.led('A','fail', 'on')
            time.sleep(1)

        print 'cylinder off -', loop
        cyt.cylinder('OFF')
        time.sleep(3)
