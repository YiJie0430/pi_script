#!/usr/bin/python
# -*- coding: UTF-8 -*-

import serial, time, sys, os
import lockProcess


'''import socket

def get_lock(process_name):
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    try:
        # The null byte (\0) means the the socket is created 
        # it works only in Linux
        get_lock._lock_socket.bind('\0' + process_name)
        return 0
    except socket.error:
        return 1'''

class rotnPlfm():
    def __init__(self, vendor):
        try:
            self.vendor = vendor
            if self.vendor == 'CYT':
                self.ser = serial.Serial('/dev/ttyUSB_FXT', 9600)
                self.ser.bytesize = 7
                self.ser.parity = 'E'
                self.opt = {'A':'1', 'B':'2', 'run':'YELLOW',
                            'pass':'GREEN', 'fail':'RED',
                            'on':'ON', 'off':'OFF'}
            elif self.vendor == 'viccom':
                self.ser = serial.Serial('/dev/ttyUSB_FXT', 9600)
                self.ser.bytesize = serial.EIGHTBITS
                self.ser.parity = serial.PARITY_NONE
                self.ser.stopbits = serial.STOPBITS_ONE
                self.opt = {'A':'1', 'B':'2', 'run':'YELLOW',
                            'pass':'GREEN', 'fail':'RED',
                            'on':'ON', 'off':'OFF'}
        except:
            self.ser = serial.Serial('/dev/ttyUSB_FXT', 9600)
            self.ser.bytesize = 7
            self.ser.parity = 'E'
            self.opt = {'A':'1', 'B':'2', 'run':'YELLOW',
                        'pass':'GREEN', 'fail':'RED',
                        'on':'ON', 'off':'OFF'}

                
    def led(self, idx, status, act):
        for state in ['RED', 'YELLOW', 'GREEN']:
            self.ser.write(b'LIGHT{}{}:OFF'.format(self.opt[idx],
                                                  state))
            time.sleep(0.1)

        self.ser.write(b'LIGHT{}{}:{}'.format(self.opt[idx],
                                             self.opt[status],
    
                                             self.opt[act]))

    def send(self, cmd, timeout=10):
        try:
            self.ser.flush()
            self.ser.write(b'{}'.format(cmd))
            self.data = str()
            st = time.time()
            while time.time() - st <= int(timeout):
                readbyte = self.ser.inWaiting()
                if readbyte:
                    self.data += self.ser.read(readbyte)
                    #print self.data
                    if '1}' in self.data or 'ASS}' in self.data:
                        self.ser.flush()
                        self.ser.close()
                        if 'HOME' in cmd or 'MOTOR:0' in cmd:
                            os.popen('echo AB > /home/pi/barcode/pos')
                        elif 'MOTOR:180' in cmd:
                            os.popen('echo BA > /home/pi/barcode/pos')
                        else:
                            pass
                        return [True, self.data]
            
            self.ser.flush()
            self.ser.close()
            return [False, self.data]
        
        except serial.SerialTimeoutException as e:
            self.ser.flush()
            self.ser.close()
            print e
            return None

    def read(self, timeout=5):
        self.data = str()
        try:
            st = time.time()
            while time.time() - st <= timeout:
                readbyte = self.ser.inWaiting()
                if readbyte:
                    self.data += self.ser.read(readbyte)
            return self.data
        except serial.SerialTimeoutException as e:
            self.ser.__del__()
            print e
            return None
    
    def alive(self):
        if self.ser.isOpen():
            return True

if __name__ == '__main__':
    start_t = time.time()
    while True:
        if time.time()-start_t <= sys.argv[2]:
            lock = lockProcess.pLock('fixture_uart')
            if not lock:
                platform = rotnPlfm(sys.argv[1])
                print (platform.send(sys.argv[2], sys.argv[3]))
                break
            else:
                print ('process running')
                time.sleep(1)
        else:
            print ('not executing')
            break
