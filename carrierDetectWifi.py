#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys, os, time, getopt, lockprocess
import RPi.GPIO as GPIO
from threading import Thread

Astate_path = '/home/pi/barcode/statusA'
Bstate_path = '/home/pi/barcode/statusB'
try:
    pin_def = {'A':[int(sys.argv[2]),22,Astate_path], 'B':[int(sys.argv[2]),24,Bstate_path]}
except:
    print ('Default IO Pin')
    pin_def = {'A':[12,22,Astate_path], 'B':[13,24,Bstate_path]}

def detect(idx):       
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_def[idx][0], GPIO.IN, pull_up_down=GPIO.PUD_OFF)
    GPIO.setwarnings(False)

    try:
        if GPIO.input(pin_def[idx][0]) == GPIO.LOW:
            print ('{} detected'.format(idx))
        else:
            print ('{} not detected'.format(idx))
                            
    except KeyboardInterrupt:
        print ('exit')
        GPIO.cleanup()
    finally:
        GPIO.cleanup()

    
if __name__ == '__main__':
    carrier = sys.argv[1]
    detect(carrier)
    
