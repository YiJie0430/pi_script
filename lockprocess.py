import os

class BackupError(Exception):
    pass

def lock(lockfile):
    def decorator(clbl):
        def wrapper(*args, **kwargs):
            try:
                # Create or fail
                os.open(lockfile, os.O_CREAT | os.O_EXCL)
            except OSError: 
                os.unlink(lockfile)
                raise BackupError("Another backup process already running."
                         "Remove `{0}` and try it again".format(lockfile))
            try:
                result = clbl(*args, **kwargs)
            finally:
                os.unlink(lockfile)
            return result
        return wrapper
    return decorator
