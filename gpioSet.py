#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys, os, time, getopt
import RPi.GPIO as GPIO

def gpioSet(*args):
   idx = None
   try:
      opts, args = getopt.getopt(args[0], 'hp:s:r', ['help','pin=','status=','read='])
   except getopt.GetoptError:
       print ('cmd err, Usage: gpioset -p <gpio pin> {-s <0:low/1:high/2:twinkle> -r}')
       sys.exit()
   for opt, arg in opts:
      if opt == '-h':
         print ('Usage: gpioset -p <gpio pin> {-s <0:low/1:high/2:twinkle> -r} ')
         sys.exit()
      elif opt in ('-p', '--pin'):
         idx = arg
      elif opt in ('-s', '--status'):
         action = arg
      elif opt in ('-r', '--read'):
         action = arg
      else:
         print ('Usage: gpioset -p <gpio pin> {-s <0:low/1:high/2:twinkle> -r}')
         sys.exit()

   status = {'high':GPIO.HIGH, 'low':GPIO.LOW, 'twinkle': 2}

   GPIO.setmode(GPIO.BCM)
   GPIO.setwarnings(False)
   
   if action == '':  
       GPIO.setup(int(idx), GPIO.OUT)
       GPIO.setwarnings(False)
       return '{}:{}'.format(idx,GPIO.input(int(idx)))
   
   elif action == 'twinkle':    
       GPIO.setup(int(idx), GPIO.OUT)
       GPIO.setwarnings(False)
       while True:
           for act in ['high', 'low']:
                GPIO.output(int(idx), status[act])
                print ('pull gpio pin%s %s'%(idx, status[act]))
                time.sleep(0.1)
   else:
       GPIO.setup(int(idx), GPIO.OUT)
       GPIO.setwarnings(False)
       GPIO.output(int(idx), status[action])
       print ('pull gpio pin%s %s'%(idx, status[action]))
   
   return action

if __name__ == '__main__':
   print (gpioSet(sys.argv[1:]))

