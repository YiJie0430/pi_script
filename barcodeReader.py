#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import serial, time, sys, getopt 

class barcode():
    def __init__(self, device, tty=None, rate=None):
        try:
            self.device=device
            #print (self.device)
            if self.device == 'unitech':
                self.ser=serial.Serial('/dev/ttyUSB_BARCODE', 9600)
                self.ser.bytesize=serial.EIGHTBITS
                self.ser.parity=serial.PARITY_NONE
                self.ser.xonxoff=True
                #self.ser.stopbits=1
                self.ser.setRTS(False)
            
            elif self.device == 'datalogic':
                self.ser=serial.Serial('/dev/ttyUSB_BARCODE', 115200)
            
            else:
                raise 'device not matched'

        except:
                self.ser=serial.Serial(tty, int(rate))
                self.ser.bytesize=serial.EIGHTBITS
                self.ser.parity=serial.PARITY_NONE
                self.ser.xonxoff=True
                #self.ser.stopbits=1
                self.ser.setRTS(False)
    
    def send(self, cmd, timeout=1):
        #print (cmd, timeout)
        self.ser.flush()
        if self.device == 'datalogic':
            self.ser.write(b'{}\r'.format(cmd))
            time.sleep(0.01)
        if self.device == 'unitech' or self.device == 'ota':
            self.ser.write(chr(0))
            time.sleep(0.05)
            self.ser.write('{}'.format(cmd.decode('hex')))
            time.sleep(0.01)
        self.data = str()
        self.waitFlag = 0
        try:
            st = time.time()
            while time.time() - st <= int(timeout):
                readbyte = self.ser.inWaiting()
                if readbyte:
                    self.data += self.ser.read(readbyte)
                else:
                    if self.waitFlag > 5 and len(set(self.data))>5:
                        break
                    self.waitFlag += 1
                    #print (self.waitFlag,len(set(self.data)))
                    time.sleep(0.1)
            return self.data
        except serial.SerialTimeoutException as e:
            print (e)
            return None

    def read(self, timeout=5):
        self.data = str()
        try:
            st = time.time()
            while time.time() - st <= timeout:
                readbyte = self.ser.inWaiting()
                if readbyte:
                    self.data += self.ser.read(readbyte)
            return self.data
        except serial.SerialTimeoutException as e:
            print (msg)
            return None

if __name__ == '__main__':
    try:
        _tty, _rate, _timeout = sys.argv[1:]
        print ('tty:{}'.format(_tty),
                'brate:{}'.format(_rate),
                'timeout:{}'.format(_timeout))
        reader = barcode(device = 'ota', tty = _tty, rate = int(_rate))
    except:
        reader = barcode(device = 'unitech')
    
    print (reader.send('04E40400FF14', _timeout))
    sys.exit()

